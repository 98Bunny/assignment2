#include<stdio.h>
int main()
{
	float n;
	printf("Enter number : ");
	scanf("%f", &n);
	if(n == 0)
	{
		printf("\nNumber is zero\n");
	}
	else if(n < 0)
	{
		printf("\nNumber is negative\n");
	}
	else if(n > 0)
	{
		printf("\nNumber is positive\n");
	}
	else
	{
		printf("\nEnter a valid number!\n");
	}
	return 0;
}
